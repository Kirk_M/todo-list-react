import React, { useState, useEffect, useRef } from "react";

function TodoForm(props) {
  const [input, setInput] = useState(props.edit ? props.edit.value : "");
  const [time, setTime] = useState(props.edit ? props.edit.time : "");
  const [timeInput, setTimeInput] = useState("");

  const inputRef = useRef(null);
  const timeInputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "text") {
      setInput(value);
    } else if (name === "time") {
      setTimeInput(value);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const newTime = timeInput !== "" ? timeInput : time;
    props.onSubmit({
      id: Math.floor(Math.random() * 10000),
      text: input,
      time: newTime,
    });
    setInput("");
    setTimeInput("");
  };

  useEffect(() => {
    if (props.edit) {
      timeInputRef.current.focus();
    }
  }, [props.edit]);

  return (
    <form onSubmit={handleSubmit} className="todo-form">
      {props.edit ? (
        <>
          <input
            placeholder="Update your item"
            value={input}
            onChange={handleChange}
            name="text"
            ref={inputRef}
            className="todo-input edit"
          />
          <input
            type="time"
            placeholder="Set task time"
            value={timeInput !== "" ? timeInput : time}
            onChange={handleChange}
            name="time"
            ref={timeInputRef}
            className="todo-input edit"
          />
          <button onClick={handleSubmit} className="todo-button edit">
            Update
          </button>
        </>
      ) : (
        <>
          <input
            placeholder="Create a task"
            value={input}
            onChange={handleChange}
            name="text"
            className="todo-input"
            ref={inputRef}
          />
          <input
            type="time"
            placeholder="Set task time"
            value={timeInput !== "" ? timeInput : time}
            onChange={handleChange}
            name="time"
            className="todo-input"
            ref={timeInputRef}
          />
          <br />
          <br />
          <button onClick={handleSubmit} className="todo-button">
            Add Task
          </button>
        </>
      )}
    </form>
  );
}

export default TodoForm;

import React, { useState } from "react";
import TodoForm from "./TodoForm";
import Todo from "./Todo";
import moment from "moment";

function TodoList() {
  const [todos, setTodos] = useState([]);

  const addTodo = (todo) => {
    if (!todo.text || /^\s*$/.test(todo.text)) {
      return;
    }

    const newTodo = {
      id: Math.floor(Math.random() * 10000),
      text: todo.text,
      time: todo.time,
      isComplete: false,
    };

    const newTodos = [newTodo, ...todos];

    setTodos(newTodos);
  };

  const updateTodo = (todoId, newValue) => {
    if (!newValue.text || /^\s*$/.test(newValue.text)) {
      return;
    }

    setTodos((prev) =>
      prev.map((item) => (item.id === todoId ? newValue : item))
    );
  };

  const removeTodo = (id) => {
    const removedArr = [...todos].filter((todo) => todo.id !== id);

    setTodos(removedArr);
  };

  const completeTodo = (id) => {
    let updatedTodos = todos.map((todo) => {
      if (todo.id === id) {
        todo.isComplete = !todo.isComplete;
      }
      return todo;
    });
    setTodos(updatedTodos);
  };

  const sortByTime = () => {
    let sortedTodos = todos.sort((a, b) => {
      return moment(a.time, "hh:mm a").diff(moment(b.time, "hh:mm a"));
    });
    setTodos([...sortedTodos]);
  };

  return (
    <>
      <h1>What's the Plan for Today?</h1>
      <TodoForm onSubmit={addTodo} />
      <div>
        <button onClick={sortByTime} className="todo-button">
          Sort by Time
        </button>
      </div>
      <Todo
        todos={todos}
        completeTodo={completeTodo}
        removeTodo={removeTodo}
        updateTodo={updateTodo}
      />
    </>
  );
}

export default TodoList;
